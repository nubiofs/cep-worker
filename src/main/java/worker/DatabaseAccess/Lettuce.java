package worker.DatabaseAccess;

import io.lettuce.core.RedisClient;
import io.lettuce.core.RedisURI;
import io.lettuce.core.ScoredValue;
import io.lettuce.core.api.StatefulRedisConnection;
import io.lettuce.core.api.sync.RedisCommands;
import io.lettuce.core.api.sync.RedisSortedSetCommands;
import org.apache.avro.Schema;
import worker.ResourcesAnalysis.ResourceAnalysis;

import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class Lettuce {

    private RedisClient redisClient;
    private StatefulRedisConnection<String, String> connection;
    private RedisCommands<String,String> syncCommands;
    private RedisSortedSetCommands<String,String> sortedSetCommands;

    /*public Lettuce(){
        String host = System.getenv("REDIS_HOST");
        redisClient = RedisClient.create("redis://"+host+":6379");
        connection = redisClient.connect();
        syncCommands = connection.sync();
        sortedSetCommands = connection.sync();
    }*/

    public Lettuce(String hostname){
        String host;
        if( hostname == null) {
            host = System.getenv("REDIS_CEP_HOST");
            String pass = System.getenv("REDIS_CEP_PASS");
            RedisURI redisUri = RedisURI.Builder.redis(host)
                    .withPassword(pass)
                    .build();
            redisClient = RedisClient.create(redisUri);
        }
        else {
            host = hostname;
            redisClient = RedisClient.create("redis://" + host + ":6379");
        }
        connection = redisClient.connect();
        syncCommands = connection.sync();
        sortedSetCommands = connection.sync();
    }

    public String getEventTypeName(String id){ return syncCommands.get(id+":Name");  } // from cataloger

    public String getEventTypeDefinition(String id){ return syncCommands.get(id+":Definition");} // from cataloger

    public Schema getAvroSchema(String id){
        String schema = syncCommands.get(id+":AvroSchema");
        Schema.Parser parser = new Schema.Parser();
        return parser.parse(schema);
    }

    public void setAvroSchema(String id,Schema schema){
        syncCommands.set(id+":AvroSchema",schema.toString());
    }

    public String getEventTypeCurrentWorker(String uuid){ return syncCommands.get(uuid+":WorkerId"); }

    public void setEventTypeCurrentWorker(String uuid, String WorkerId){
        String previousWorkerId = syncCommands.get(uuid + ":WorkerId");
        syncCommands.set(uuid+":WorkerId",WorkerId);
        syncCommands.sadd("WorkerId"+WorkerId+"EventTypes",uuid);
        if(previousWorkerId!=null){
            syncCommands.srem("WorkerId"+previousWorkerId+"EventTypes",uuid);
        }
    }

    public boolean EventTypeCurrentWorker(String uuid, String WorkerId){
        return syncCommands.get(uuid+":WorkerId").equalsIgnoreCase(WorkerId);
    }

    public Set<String> getEventTypeInputs(String id){   // from cataloger
        return syncCommands.smembers(id+":Inputs");
    }

    public void eventTypeStateBuildedAdd(String uuid, String WorkerId){
        syncCommands.sadd(uuid+":StateBuilded",WorkerId);
    }

    public boolean eventTypeStateBuilded(String uuid, String WorkerId){
        return syncCommands.sismember(uuid+":StateBuilded",WorkerId);
    }

    public void eventTypeStateBuildedDel(String uuid, String WorkerId){
        syncCommands.srem(uuid+":StateBuilded",WorkerId);
    }

    public void eventTypeAssigned(String uuid){
        //syncCommands.zpopmin("Unnasigned");
        //syncCommands.srem("Unnasigned",uuid);
        syncCommands.sadd("Assigned",uuid);
    }

    public List<String> getUnnasignedEventTypes(){
        return syncCommands.zrange("Unnasigned",0,-1);
    }

    public String getUnnasignedEventType(){

        String uuid;
        ScoredValue<String> result = syncCommands.zpopmin("Unnasigned");

        if(!result.hasValue())
            return null;
        else
            //uuid = result.getValue();
            //syncCommands.srem("RellocationPile",uuid);
            return result.getValue();
    }

    public boolean hasUnnasignedEvents(){
        long UnnasignedTypes = syncCommands.zcard("Unnasigned");
        return UnnasignedTypes > 0;
    }

    public void pushEventTypeForRellocation(String uuid,Double score){
        //syncCommands.sadd("RellocationPile",uuid);
        syncCommands.zadd("RellocationStack",score,uuid);
    }

    public void undoPushEventTypeForRellocation(String uuid){
        //syncCommands.srem("RellocationPile",uuid);
        syncCommands.zrem("RellocationStack",uuid);
    }

    public boolean hasEventTypeBeenPickedUpForRellocation(String uuid, String WorkerId){
        return syncCommands.get(uuid + ":ReceveingWorkerId") != null;

        
        //System.out.println("Receiving worker ID: "+syncCommands.get(uuid+":ReceveingWorkerId"));
        //System.out.println("Current worker id : "+WorkerId);
        //return !WorkerId.equals(syncCommands.get(uuid+":ReceveingWorkerId"));
        //Double hasd = syncCommands.zscore("RellocationStack", uuid);
        //return syncCommands.zscore("RellocationStack", uuid) != null;
    }

    public String popEventTypeForAcceptingRellocation(){
        //return sortedSetCommands.zrevrange("RellocationStack",0,1).iterator().next();
        String uuid;
        ScoredValue<String> result = sortedSetCommands.zpopmax("RellocationStack");
        if(!result.hasValue())
            return null;
        else
            //uuid = result.getValue();
            //syncCommands.srem("RellocationPile",uuid);
            return result.getValue();
    }

    public String getEventTypeReceiveingWorker(String uuid){
        return syncCommands.get(uuid+":ReceveingWorkerId");
    }

    public void removeEventTypeReceveingWorker(String uuid){
        syncCommands.del(uuid+":ReceveingWorkerId");
    }

    public void setEventTypeReceveingWorker(String uuid, String WorkerId){
        syncCommands.set(uuid+":ReceveingWorkerId",WorkerId);
    }

    public Set<String> getDeletedEventTypes(Set<String> currentEventTypes, String Workerid){
        Set<String> deletedIds = new HashSet<>();
        Set<String> AssignedIds = syncCommands.smembers("Assigned");
        for(String TypeId : currentEventTypes){
            if(!AssignedIds.contains(TypeId)){
                deletedIds.add(TypeId);
            }
        }
        return deletedIds;
    }

    public Set<String> getAllEventTypesOfWorker(String WorkerId){
        return syncCommands.smembers("WorkerId"+WorkerId+"EventTypes");
    }

    public Set<String> getWorkerPods(){
        return syncCommands.smembers("Pods");
    }

    public void addWorkerPod(String WorkerId){
        syncCommands.sadd("Workers",WorkerId);
        syncCommands.sadd("PodCreationTime",String.valueOf(System.currentTimeMillis()));
    }

    public int getTotalNumberOfWorkers(){
        return syncCommands.scard("Workers").intValue();
    }
    public void updateWorkerState(String WorkerId){
        if(ResourceAnalysis.softOverload())
            syncCommands.sadd("FullWorkers",WorkerId);
        else
            syncCommands.srem("FullWorkers",WorkerId);
    }

    public int getNumberOfFullWorkers(){
        return syncCommands.scard("FullWorkers").intValue();
    }

    public void setemptyworker(String WorkerId){ syncCommands.srem("FullWorkers",WorkerId); }

    public void deleteWorkerPod(String PodId){
        syncCommands.srem("Pods",PodId);
        syncCommands.sadd("PodExclusionTime",String.valueOf(System.currentTimeMillis()));
    }

    public void Close(){
        connection.close();
        redisClient.shutdown();

    }

}
