package worker.ClusterHandler;

public interface ClusterHandler {

    boolean instantiateNewWorker(String hostname);

    void StopAndRemoveWorkerFromCluster(String WorkerId, String hostname);
}
