package worker;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import worker.DatabaseAccess.Lettuce;
import worker.ThreadHandler.ThreadHandler;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class Processor {

    private static Logger LOG = LoggerFactory.getLogger(Processor.class);

    private static final boolean logs_p = Boolean.parseBoolean(System.getenv("LOGS_PROCESSOR"));

    public static void main(String[] args) {

        String WorkerId = null;
        String mode = null;

        /*if (args.length < 1) {
            System.out.print("Worker Id not provided");
            System.exit(0);
        }

        WorkerId = args[0];

        if (args.length < 2) {
            System.out.print("Runtime mode not provided");
            System.exit(0);
        }

        mode = args[1];
        */

        ExecutorService executor  = Executors.newCachedThreadPool();

        WorkerId = System.getenv("WORKER_ID");
        mode = System.getenv("MODE");

        Lettuce store = new Lettuce(null);
        store.addWorkerPod(WorkerId);
        store.Close();


        LOG.info("Creating Thread Handler");
        final ThreadHandler threadHandler = new ThreadHandler(WorkerId,null, executor);

        if(logs_p )System.out.print("P | Worker Started\n");

        //For single node execution
        if(mode.equalsIgnoreCase("mono"))
            threadHandler.ContinuousSingleProcessing();

        //For multi node execution
        else {
            if(logs_p )System.out.println("P | Starting worker on multi node environment");
            //thread for creating threads that accept new eventTypes
            executor.submit(() -> {threadHandler.InitiateAcceptionOfEvents();});
            if(logs_p )System.out.println("P | Initiate Acception Of Events");

            // Thread to relocate events to other workers in case of Underload
            executor.submit(() -> {threadHandler.InitiateUnderloadMonitoring();});
            if(logs_p )System.out.println("P | Initiate Underload Monitoring");

            //Thread to rellocate events to other workers in case of Overload
            executor.submit(() -> {threadHandler.InitiateOverloadMonitoring();});
            if(logs_p )System.out.println("P | Initiate Overload Monitoring");
        }
    }
}
