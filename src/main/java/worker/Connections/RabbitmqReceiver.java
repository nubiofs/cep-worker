package worker.Connections;

import com.rabbitmq.client.*;
import java.io.IOException;
import java.util.concurrent.TimeoutException;


import org.apache.avro.Schema;
import org.apache.avro.generic.GenericData.Record;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import worker.Events.EventHandler;
import worker.utils.Serializer;

import static java.lang.System.exit;

public class RabbitmqReceiver implements Receiver{
    private String queue_name;
    private static final String exchange_name = System.getenv("EXCHANGE");
    private static final String host = System.getenv("RABBITMQ_HOST");
    private static final String user = System.getenv("RABBITMQ_USERNAME");
    private static final String pass = System.getenv("RABBITMQ_PASSWORD");
    private Connection connection;
    private Channel channel;
    private final Consumer consumer;
    private int eventsReceivedperPeriod;
    //private static Logger LOG = LoggerFactory.getLogger(RabbitmqReceiver.class);
    private static final boolean logs_receiver = Boolean.parseBoolean(System.getenv("LOGS_RECEIVER"));


    public RabbitmqReceiver(Schema schema,String TypeId,String TypeName,final EventHandler eventHandler,Connection c) {
        eventsReceivedperPeriod = 0;
        try {
            //connection = c;
            channel = c.createChannel();
            if(logs_receiver) System.out.println("Receiver | new channel "+channel.getChannelNumber());
            channel.exchangeDeclare(exchange_name, "topic");
            if(logs_receiver) System.out.println("Receiver | declaring exchange : "+exchange_name);
            queue_name = channel.queueDeclare().getQueue();
            if(logs_receiver) System.out.println("Receiver | new queue "+queue_name);
            channel.queueBind(queue_name, exchange_name, TypeId);
            if(logs_receiver) System.out.println("Receiver | binding queue to channel");
        } catch (IOException e) {

            e.printStackTrace();
        }

        //if(channel.isOpen()) LOG.info("Channel open on creation\n");
        consumer = new DefaultConsumer(channel) {
            @Override
            public void handleDelivery(String consumerTag, Envelope envelope, AMQP.BasicProperties properties, byte[] body) throws IOException {
                Record event = Serializer.AvroDeserialize(body, schema);
                eventHandler.handle(event, TypeName);
                long deliveryTag = envelope.getDeliveryTag();
                //channel.basicAck(deliveryTag, false);
            }
        };

        try {
            channel.basicConsume(queue_name, true, "myConsumerTag", consumer );
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    public RabbitmqReceiver(Schema schema,String TypeId,String TypeName,final EventHandler eventHandler,Channel c) {

        eventsReceivedperPeriod = 0;
        try {
            channel = c;
            queue_name = channel.queueDeclare().getQueue();
            if(logs_receiver) System.out.println("Receiver | new queue "+queue_name);
            channel.queueBind(queue_name, exchange_name, TypeId);
            if(logs_receiver) System.out.println("Receiver | binding queue to channel");
        } catch (IOException e) {
            e.printStackTrace();
        }

        //if(channel.isOpen()) LOG.info("Channel open on creation\n");
        consumer = new DefaultConsumer(channel) {
            @Override
            public void handleDelivery(String consumerTag, Envelope envelope, AMQP.BasicProperties properties, byte[] body) throws IOException {
                Record event = Serializer.AvroDeserialize(body, schema);
                eventHandler.handle(event, TypeName);
                long deliveryTag = envelope.getDeliveryTag();
                //channel.basicAck(deliveryTag, false);
            }
        };

        try {
            channel.basicConsume(queue_name, true, TypeId+"t"+queue_name, consumer );
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    public RabbitmqReceiver(Schema schema,String TypeId,String TypeName,final EventHandler eventHandler) {

        eventsReceivedperPeriod = 0;


        ConnectionFactory factory = new ConnectionFactory();
        factory.setHost(host);
        factory.setUsername(user);
        factory.setPassword(pass);
        try {
            connection = factory.newConnection();
            if(logs_receiver) System.out.println("Receiver | new connection "+connection.getId()+" from factory ");
            channel = connection.createChannel();
            if(logs_receiver) System.out.println("Receiver | new channel "+channel.getChannelNumber());
            channel.exchangeDeclare(exchange_name, "topic");
            if(logs_receiver) System.out.println("Receiver | declaring exchange : "+exchange_name);
            queue_name = channel.queueDeclare().getQueue();
            if(logs_receiver) System.out.println("Receiver | new queue "+queue_name);
            channel.queueBind(queue_name, exchange_name, TypeId);
            if(logs_receiver) System.out.println("Receiver | binding queue to channel");
        } catch (IOException | TimeoutException e) {
            e.printStackTrace();
        }

        //if(channel.isOpen()) LOG.info("Channel open on creation\n");
        consumer = new DefaultConsumer(channel) {
            @Override
            public void handleDelivery(String consumerTag, Envelope envelope, AMQP.BasicProperties properties, byte[] body) throws IOException {
                Record event = Serializer.AvroDeserialize(body, schema);
                eventHandler.handle(event, TypeName);
                long deliveryTag = envelope.getDeliveryTag();
                //channel.basicAck(deliveryTag, false);
            }
        };

        try {
            channel.basicConsume(queue_name, true, "myConsumerTag", consumer );
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    public RabbitmqReceiver(Schema schema, String xg,String TypeId, String TypeName, Channel channel, final EventHandler eventHandler) {
        //LOG.info("Creating Rabbitmq receiver connection");

        ;
        try {
            channel.exchangeDeclare(xg, "topic");
            queue_name = channel.queueDeclare().getQueue();
            channel.queueBind(queue_name, xg, TypeId);
        } catch (IOException e) {
            e.printStackTrace();
        }
        this.channel = channel;
        //if(channel.isOpen()) LOG.info("Channel received on creation\n");

        consumer = new DefaultConsumer(channel) {
            @Override
            public void handleDelivery(String consumerTag, Envelope envelope, AMQP.BasicProperties properties, byte[] body) {
                Record event = Serializer.AvroDeserialize(body, schema);
                eventHandler.handle(event, TypeName);
                eventsReceivedperPeriod++;
            }
        };
        try {
            channel.basicConsume(queue_name,true,"12344",false,true,null,consumer);
            //channel.basicConsume(queue_name, true, consumer);
        } catch (IOException | ShutdownSignalException e) {
            e.printStackTrace();
        }
    }


    public void CloseConnection()  {
        //if(channel.isOpen()) LOG.info("Channel marked for closing\n");
        try {
            channel.close();
            connection.close();
        } catch (IOException | TimeoutException e) {
            e.printStackTrace();
        }
    }


    public int eventsPerPeriod(){
        //if(channel.isOpen()) System.out.print("Channel open for counting\n");
        int i = eventsReceivedperPeriod;
        eventsReceivedperPeriod = 0;
        return i;
    }

    void Reroute(Consumer consumer) {
        //if(channel.isOpen()) LOG.info("Channel open for rerouting\n");
        try {
            channel.basicConsume(queue_name, true, consumer);
        } catch (IOException e) {
            e.printStackTrace();
        }
        eventsReceivedperPeriod++;
    }

}
