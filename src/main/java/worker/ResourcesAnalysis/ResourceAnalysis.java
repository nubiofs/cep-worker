package worker.ResourcesAnalysis;


import javax.management.*;
import java.lang.management.ManagementFactory;

public class ResourceAnalysis {

    private static boolean logs_ra = Boolean.parseBoolean(System.getenv("LOGS_RESOURCEANALYSIS"));

    private static double getProcessCpuLoad() {
        double percentage = Double.NaN;
        while(percentage == Double.NaN) {

            MBeanServer mbs = ManagementFactory.getPlatformMBeanServer();
            ObjectName name = null;
            AttributeList list = null;
            try {
                name = ObjectName.getInstance("java.lang:type=OperatingSystem");
                list = mbs.getAttributes(name, new String[]{"ProcessCpuLoad"});
            } catch (MalformedObjectNameException | InstanceNotFoundException | ReflectionException e) {
                e.printStackTrace();
            }
            Double value = Double.NaN;
            if (!list.isEmpty()) {
                Attribute att = (Attribute) list.get(0);
                value = (Double) att.getValue();

                // usually takes a couple of seconds before we get real values
                if (value != -1.0) {
                    percentage = value;
                }
            }
            // returns a percentage value with 1 decimal point precision
        }
        if(logs_ra) System.out.println("RA | CPU LOAD : "+((int) (percentage * 1000) / 10.0)+" from 0 to 1");
        return ((int) (percentage * 1000) / 10.0);
    }

    public static void printmemory(){
        double maxmemory = (double) Runtime.getRuntime().maxMemory();
        double usedmemory = (double) Runtime.getRuntime().totalMemory() - Runtime.getRuntime().freeMemory();
        System.out.println("max memory = "+maxmemory);
        System.out.println("used memory = "+usedmemory);
    }


    private static double getMemoryUsage(){
        double totalMemory = (double) Runtime.getRuntime().totalMemory();  // in MB
        double usedMemory = totalMemory - Runtime.getRuntime().freeMemory();
        double usedPercent;
        double maxMemory;

        maxMemory = (double) Runtime.getRuntime().maxMemory();
        usedPercent = usedMemory / maxMemory;

        if (logs_ra) {
            System.out.println("RA | MEMORY LOAD : " + usedPercent + " from 0 to 1");
            System.out.println("RA | MAX MEMORY : " + maxMemory);
            System.out.println("RA | TOTAL MEMORY : " + totalMemory);
            System.out.println("RA | USED MEMORY : " + usedMemory);

        }
        return usedPercent;
    }



    public static boolean softOverload(){
        boolean cpuoverload = getProcessCpuLoad() > Double.parseDouble(System.getenv("SOFT_OVERLOAD"));
        boolean memoryoverload = getMemoryUsage() > Double.parseDouble(System.getenv("SOFT_OVERLOAD"));
        return cpuoverload || memoryoverload;
    }


    public static boolean hardOverload(){
        boolean cpuoverload = getProcessCpuLoad() > Double.parseDouble(System.getenv("HARD_OVERLOAD"));
        boolean memoryoverload = getMemoryUsage() > Double.parseDouble(System.getenv("HARD_OVERLOAD"));
        return cpuoverload || memoryoverload;
    }

    public static boolean Underload(){
        boolean cpuUnderload = getProcessCpuLoad() < Double.parseDouble(System.getenv("UNDERLOAD"));
        boolean memoryUnderload = getMemoryUsage() < Double.parseDouble(System.getenv("UNDERLOAD"));
        return cpuUnderload && memoryUnderload;
    }

    public static double getResourceUsage(){
        double cpuload = getProcessCpuLoad();
        double memoryload = getMemoryUsage();
        return Math.max(memoryload, cpuload);
    }
}
