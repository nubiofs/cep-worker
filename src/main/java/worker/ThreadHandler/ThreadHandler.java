package worker.ThreadHandler;

import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;
import com.rabbitmq.client.ConnectionFactory;
import org.apache.avro.Schema;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import worker.ClusterHandler.ClusterHandler;
import worker.ClusterHandler.KubernetesHandler;
import worker.Connections.*;
import worker.DatabaseAccess.Lettuce;
import worker.Events.EventHandler;
import worker.Events.QueryAnalysis;
import worker.LoadBalancers.InputSimilarity;
import worker.LoadBalancers.StateUsage;
import worker.LoadBalancers.LoadBalancer;
import worker.ResourcesAnalysis.ResourceAnalysis;

import java.io.IOException;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.concurrent.*;


public class ThreadHandler {
    private static final Logger LOG = LoggerFactory.getLogger(ThreadHandler.class);

    private static final String Broker = System.getenv("BROKER");

    private static final String LoadBalancingAlgorithm = System.getenv("LBA");

    private ClusterHandler workerHandler;

    private volatile EventHandler eventHandler;

    private ExecutorService executor;
    private final String WorkerId;
    private volatile LoadBalancer loadBalancer;

    private static final String exchange_name = System.getenv("EXCHANGE");

    private static final String rmode = System.getenv("RMODE");

    private static final boolean logs_th = Boolean.parseBoolean(System.getenv("LOGS_THREADHANDLER"));

    private static final boolean logs_o = Boolean.parseBoolean(System.getenv("LOGS_OVERLOAD"));
    private static final boolean logs_u = Boolean.parseBoolean(System.getenv("LOGS_UNDERLOAD"));
    private static final boolean logs_i = Boolean.parseBoolean(System.getenv("LOGS_INITIATE"));
    private static final boolean logs_relo = Boolean.parseBoolean(System.getenv("LOGS_RELOCATION"));
    private static final boolean logs_deletion = Boolean.parseBoolean(System.getenv("LOGS_DELETION"));
    private static final boolean logs_add = Boolean.parseBoolean(System.getenv("LOGS_INCLUSION"));
    private ConcurrentHashMap<String, Receiver> ReceiverList;
    private ConcurrentHashMap<String, Sender> SenderList;

    private ConcurrentHashMap<String,Integer> counters;

    //private RabbitmqSender sender;

    private final String RedisHost;

    private Connection connR;
    private Connection connS;
    private Channel channelS;
    private Channel channelR;


    public ThreadHandler(String WorkerID,String redishost, ExecutorService executor){
        LOG.debug("Creating Event Handler\n"); // Translate EPL statements into triggers
        this.eventHandler = new EventHandler();
        LOG.debug("Creating Event Sender\n "); // Class for sending events to RabbitMQ

        this.executor = executor;

        LOG.debug("Creating Event Receivers\n"); // Class for receiving events from other instances
        this.ReceiverList = new ConcurrentHashMap<>();
        this.SenderList = new ConcurrentHashMap<>();


        this.WorkerId = WorkerID;

        this.RedisHost = redishost;

        if(logs_th) System.out.println("TH | Max memory: "+(double) Runtime.getRuntime().maxMemory());


        if(logs_th) System.out.print("TH | Chosen Load Balancing Algorithm: "+LoadBalancingAlgorithm+"\n");
        if(("inputsimilarity").equals(LoadBalancingAlgorithm)) {
            this.loadBalancer = new InputSimilarity(RedisHost);
        }
        else if(("stateusage").equals(LoadBalancingAlgorithm)){
            this.loadBalancer = new StateUsage(RedisHost);
        }

        
        this.workerHandler = new KubernetesHandler();

        //Class for storing counters for rellocation purposes
        this.counters = new ConcurrentHashMap<>();

        if(logs_th) System.out.print("TH | Chosen Broker: "+Broker+"\n");
        if(("Rabbitmq").equals(Broker)&& (rmode.equals("conn")||rmode.equals("channel"))) {
            ConnectionFactory factory = new ConnectionFactory();
            factory.setHost(System.getenv("RABBITMQ_HOST"));
            factory.setUsername(System.getenv("RABBITMQ_USERNAME"));
            factory.setPassword(System.getenv("RABBITMQ_PASSWORD"));
            factory.setRequestedChannelMax(65535);
            try {
                connS = factory.newConnection();
                connR = factory.newConnection();
                if(logs_th) System.out.println("TH | Rabbitmq Connection in open ");
                if(logs_th) System.out.println("TH | Maximum number of channels: "+connR.getChannelMax());
                //if(logs_th) System.out.println("TH | Chaneel in open: "+channel.isOpen());
            } catch (IOException | TimeoutException e) {
                e.printStackTrace();
            }
            if(rmode.equals("channel")) {
                try {
                    channelS = connS.createChannel();
                    channelR = connR.createChannel();
                    //if(logs_th) System.out.println("TH | Chaneel in open: "+channel.isOpen());
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }

        }


        


    }




    public void ContinuousSingleProcessing(){
        System.out.print("Mono | Single processing starting\n");

        Lettuce store = new Lettuce(RedisHost);
        System.out.print("Mono | Begin Event Type Assignment\n");
        boolean status = true;
        while (status) {
            if (store.hasUnnasignedEvents()) {
                List<String> EventTypeIds = store.getUnnasignedEventTypes();
                System.out.print("mono | Adding event types in thread handler\n");
                for (String EventTypeId : EventTypeIds) {
                    System.out.print("Mono | Adding event typeId:"+EventTypeId+"\n");
                    AddOutgoingEventType(EventTypeId,store);
                }
            }

            try {
                TimeUnit.MINUTES.sleep(15);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }

    }

    /*public void RecuperatingPossibleLostEventTypes(){
        System.out.print("Checking Worker:"+WorkerId+" for previous event types\n");
        executor.submit(() -> {
            Lettuce store = new Lettuce(RedisHost);
            Set<String> lostEventTypes= store.getAllEventTypesOfWorker(WorkerId);

            while(!lostEventTypes.isEmpty()) {
                for (String TypeId : lostEventTypes) {
                    if (!ResourceAnalysis.Overload()) {
                        AddOutgoingEventType(TypeId, store);
                        lostEventTypes.remove(TypeId);
                    }
                }
                try {
                    Thread.sleep(Integer.parseInt(System.getenv("TIMEOUT_A")));
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }

        });
    }

     */

    public void InitiateAcceptionOfEvents(){
        if(logs_i )System.out.println("I  | Initiating accepting events on Worker "+WorkerId);

        Lettuce stor = new Lettuce(RedisHost);

        Set<String> lostEventTypes= stor.getAllEventTypesOfWorker(WorkerId);
        if(logs_i)System.out.println("I  | lost event types: "+lostEventTypes.toString());
        if(lostEventTypes.isEmpty() && logs_i)
            System.out.println("I  | No events lost from previous session");
        else {
            stor.setemptyworker(WorkerId);
            if(logs_i) { System.out.println("I  | Retrivieng event types from previous session");}

            while (!lostEventTypes.isEmpty()) {
                for (String TypeId : lostEventTypes) {
                    if (!ResourceAnalysis.softOverload()) {
                        AddOutgoingEventType(TypeId, stor);
                        lostEventTypes.remove(TypeId);
                    }
                }
                try {
                    Thread.sleep(10000);
                } catch (InterruptedException e) {
                    if(logs_i ) System.out.println("I  | Thread Failed to Sleep");
                    e.printStackTrace();
                }
            }
        }
        stor.Close();
        executor.submit(() -> {
            LOG.info("I  | Accepting event types");
            if(logs_i ) System.out.println("I  | Accepting event types");
            boolean status = true;
            //boolean full_worker = false;
            while (status) {
                Lettuce store = new Lettuce(RedisHost);
                if(logs_i) System.out.println("I  | Checking soft Overload");



                String EventTypetoAcceptRealocation = store.popEventTypeForAcceptingRellocation();
                if(EventTypetoAcceptRealocation != null && !eventHandler.getEventTypeIds().contains(EventTypetoAcceptRealocation)) {
                    while (!EventTypetoAcceptRealocation.isEmpty() && !ResourceAnalysis.softOverload()) {
                        if(logs_i ) System.out.println("I  | Accepting realocated event type " + EventTypetoAcceptRealocation);
                        AcceptRelocation(EventTypetoAcceptRealocation);
                        EventTypetoAcceptRealocation = store.popEventTypeForAcceptingRellocation();
                    }
                }


                while(!ResourceAnalysis.softOverload()&&store.hasUnnasignedEvents()) {
                    if(logs_i ) System.out.println("I  | Accepting new event types");
                    //System.out.println("I  | Adding new event Type - Current Number of Event Types:"+eventHandler.getEventTypeIds().size());
                    AddUnnasignedEventType(store.getUnnasignedEventType(), store);
                }
                store.updateWorkerState(WorkerId);
                if(logs_i) System.out.println("I  | Resource usage: "+ResourceAnalysis.getResourceUsage());


                if(logs_o) System.out.println("I  | number of full workers "+store.getNumberOfFullWorkers());
                if(logs_o) System.out.println("I  | number of workers "+store.getTotalNumberOfWorkers());
                if(logs_o) ResourceAnalysis.printmemory();
                //if(logs_o) System.out.println("I  | number of Unnasigned Event Types "+store.hasUnnasignedEvents());
                if((store.getNumberOfFullWorkers()==store.getTotalNumberOfWorkers())&&store.hasUnnasignedEvents()){
                    if(logs_o) System.out.println("I  | All workers are full - starting new instance");
                    workerHandler.instantiateNewWorker(RedisHost);
                }

                store.Close();
                int timeout_a = Integer.parseInt(System.getenv("TIMEOUT_A"));
                if(logs_i ) System.out.println("I  | Timeout "+timeout_a/(1000*60)+" minutes");

                try {
                    Thread.sleep(timeout_a);
                } catch (InterruptedException e) {
                    if(logs_i ) System.out.println("I  | Thread Failed to Sleep");
                    e.printStackTrace();
                    //status = false;
                }

            }

        });
    }

    public void InitiateUnderloadMonitoring(){
        executor.submit(() -> {
            boolean status = true;
            while (status) {
                Lettuce store = new Lettuce(RedisHost);

                Set<String> deletedEventTypes = store.getDeletedEventTypes(SenderList.keySet(),WorkerId);
                if(!deletedEventTypes.isEmpty()) {
                    if(logs_u )System.out.println("U  | deleting old Event types checks"+deletedEventTypes);
                    for (String deletedTypeId : deletedEventTypes) {
                        //if(logs_u )System.out.println("U  | deleting Event types checks "+deletedTypeId);
                        deleteCheckExpressionAndUnnecessaryInputs(store, deletedTypeId);
                    }
                }
                store.Close();
                try {
                    int timeout_u = Integer.parseInt(System.getenv("TIMEOUT_U"));
                    if(logs_u ) System.out.println("U  | Timeout "+timeout_u/(1000*60)+" minutes");
                    if(logs_u) System.out.println("U  | Resource usage: "+ResourceAnalysis.getResourceUsage());
                    Thread.sleep(timeout_u);
                } catch (InterruptedException e) {
                    if(logs_u ) System.out.println("U  | Thread failed to sleep");
                    e.printStackTrace();
                    //status = false;
                }
                int tentatives = 0;
                boolean tryAnotherTime = false;
                if(ResourceAnalysis.Underload())
                    if(logs_u ) System.out.println("U  | Low resurce usage: Realocating all event types and shutting down worker");
                while(ResourceAnalysis.Underload()&&!tryAnotherTime) {
                    if(ReceiverList.isEmpty()){
                        workerHandler.StopAndRemoveWorkerFromCluster(WorkerId, RedisHost);
                    }
                    String uuid = loadBalancer.findEventTypetoRellocate();
                    boolean tentative = Relocate(ResourceAnalysis.getResourceUsage(),uuid);
                    if(!tentative) tentatives++;
                    if(tentatives>3) {
                        if(logs_u ) System.out.println("U  | too much failed tentatives for realocation");
                        tryAnotherTime=true;
                    }
                }
            }
        });
    }

    public void InitiateOverloadMonitoring(){
        executor.submit(() -> {
            boolean status = true;
            while (status) {
                try {
                    int timeout_o = Integer.parseInt(System.getenv("TIMEOUT_O"));
                    if(logs_o )System.out.println("O  | Timeout "+timeout_o/(1000*60)+" minutes");
                    if(logs_o) System.out.println("O  | Resource usage: "+ResourceAnalysis.getResourceUsage());
                    Thread.sleep(timeout_o);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                    //status = false;
                }
                Lettuce store = new Lettuce(RedisHost);
                Set<String> deletedEventTypes = store.getDeletedEventTypes(SenderList.keySet(),WorkerId);
                if(!deletedEventTypes.isEmpty()) {
                    if(logs_o )System.out.println("O  | deleting old Event types checks"+deletedEventTypes);
                    for (String deletedTypeId : deletedEventTypes) {
                        //if(logs_u )System.out.println("U  | deleting Event types checks "+deletedTypeId);
                        deleteCheckExpressionAndUnnecessaryInputs(store, deletedTypeId);
                    }
                }




                store.Close();
                while (ResourceAnalysis.hardOverload()) {
                    if(logs_o )System.out.println("O  | High resurce usage: Realocating event types ");

                    String typeToRellocate = loadBalancer.findEventTypetoRellocate();
                    boolean newWorkerInstantiaded = false;
                    boolean RealocationStarted = false;
                    for(int i = 0 ; i < 3 && !RealocationStarted ; i++)
                         RealocationStarted = Relocate(ResourceAnalysis.getResourceUsage(), typeToRellocate);
                    if(!RealocationStarted && logs_o)
                        System.out.println("O  | Failed to realocate to existing worker");
                    while(!RealocationStarted){
                        newWorkerInstantiaded = workerHandler.instantiateNewWorker(RedisHost);
                        if(newWorkerInstantiaded) {
                            if(logs_o ) System.out.println("O  | Instatianting new Worker");
                            try {
                                Thread.sleep(1000);
                            } catch (InterruptedException e) {
                                if(logs_o ) System.out.println("O  | Thread failed to sleep");
                                e.printStackTrace();
                            }
                        }
                        RealocationStarted = Relocate(ResourceAnalysis.getResourceUsage(), typeToRellocate);
                    }
                }
                store.updateWorkerState(WorkerId);

            }
        });
    }

    //private void AcceptRellocation(String OutputTypeID){ AcceptRelocation(OutputTypeID); }



    private void AcceptRelocation(String OutputTypeID){


        Lettuce store = new Lettuce(RedisHost);

        String SendingWorkerID = setUpReceivingAndSendingWorkerIds(store,OutputTypeID);
        if(logs_relo )System.out.println("AcceptRelocation | Accepting Realocation of Event type: "+OutputTypeID);

        addEventTypeToThreadHandler(OutputTypeID);
        if(logs_relo )System.out.println("AcceptRelocation | Adding Event type "+OutputTypeID+" to Thread Handler");

        addEventTypeToRuntime(store,OutputTypeID);
        if(logs_relo )System.out.println("AcceptRelocation | Adding Event type "+OutputTypeID+" to Runtime");

        //Evaluating time and events needed to build state
        long timeToWait = evaluateInputsToBuildState(store,OutputTypeID);
        if(logs_relo )System.out.println("AcceptRelocation | Time to wait for realocation of Event type("+OutputTypeID+"): "+timeToWait+" seconds");

        //building state
        buildStateAndSave(store,OutputTypeID,timeToWait);
        if(logs_relo )System.out.println("AcceptRelocation | Building State for Event type "+OutputTypeID);

        //waiting for other worker to stop sending events
        waitSendingWorkerNotification(OutputTypeID,SendingWorkerID,store);
        if(logs_relo )System.out.println("AcceptRelocation | Waiting for other worker to finish processing of Event Type "+OutputTypeID);

        //reset current worker in db
        startSendingEvents(OutputTypeID);
        if(logs_relo )System.out.println("AcceptRelocation | Start detecting Event type "+OutputTypeID);

        //update load balancer rank
        loadBalancer.addEventTypeToRank(OutputTypeID,ReceiverList);
        if(logs_relo )System.out.println("AcceptRelocation | Update Load Balancer with Event type "+OutputTypeID);

        store.Close();
    }

    private String setUpReceivingAndSendingWorkerIds(Lettuce store, String OutputTypeID){
        store.setEventTypeReceveingWorker(OutputTypeID,WorkerId);

        return store.getEventTypeCurrentWorker(OutputTypeID);
    }
    private void addEventTypeToThreadHandler(String OutputTypeID){
        //Creating new Sender and stopping from sending events

        Sender sender = NewSender();
        sender.stopSending();
        SenderList.put(OutputTypeID,sender);

    }
    private void addEventTypeToRuntime(Lettuce store, String OutputTypeID){
        String Name = store.getEventTypeName(OutputTypeID);
        String Query = store.getEventTypeDefinition(OutputTypeID);
        Set<String> inputs = store.getEventTypeInputs(OutputTypeID);
        String input_name;
        Schema input_schema;
        for(String s : inputs){
            input_name = store.getEventTypeName(s);
            input_schema = store.getAvroSchema(s);
            eventHandler.addInputStream(input_name,input_schema);
        }
        eventHandler.addCheckExpression(OutputTypeID,Name, Query, SenderList.get(OutputTypeID));
        store.setAvroSchema(OutputTypeID,eventHandler.getSchema(OutputTypeID));
    }
    private long evaluateInputsToBuildState(Lettuce store,String OutputTypeID){
        Set<String> neoIncomingIds;
        String Query = store.getEventTypeDefinition(OutputTypeID);
        long timeToWait = 0;
        neoIncomingIds = store.getEventTypeInputs(OutputTypeID);
        for(String id : neoIncomingIds){
            if(!ReceiverList.containsKey(id)){
                CreateInputReceiver(id);
            }
            String name = store.getEventTypeName(id);
            if(QueryAnalysis.hasDataWindow(Query,name)) {
                String stype = QueryAnalysis.getStateType(Query, name);
                String snumber = QueryAnalysis.getStateNumber(Query, name);
                QueryAnalysis.analyzeQueryLengh(stype, snumber, id, counters);
                //only accept value in seconds ( e.g.(3))
                timeToWait += QueryAnalysis.analyzeQueryTime(stype, snumber);
            }
        }
        return timeToWait;
    }
    private void buildStateAndSave(Lettuce store, String OutputTypeID,long timeToWait){
        boolean countersRemoved = false;
        boolean timePassed = false;
        while(!countersRemoved && !timePassed){ //waiting for state to build
            if(counters.isEmpty()) {
                countersRemoved = true;
            }
            try {
                Thread.sleep(timeToWait*1000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            timePassed = true;
        }
        store.eventTypeStateBuildedAdd(OutputTypeID,WorkerId);

    }
    private void waitSendingWorkerNotification(String OutputTypeID,String SendingWorkerID,Lettuce store){
        boolean transferConfirmed = false;
        while(!transferConfirmed){
            if(!store.EventTypeCurrentWorker(OutputTypeID,SendingWorkerID))
                transferConfirmed = true;
            try {
                Thread.sleep(10000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
        //store.removeEventTypeReceveingWorker(OutputTypeID);
    }
    private void startSendingEvents(String OutputTypeID){
        SenderList.get(OutputTypeID).restartSending();
    }



    private boolean Relocate(double resourceusage, String EventTypeId){
        if(logs_relo )System.out.println("Realocation | Pushing Event type "+EventTypeId+" to Relocation to other worker");
        Lettuce store = new Lettuce(RedisHost);

        putEventForRelocation(store,resourceusage,EventTypeId);

        if(logs_relo )System.out.println("Realocation | Waiting for other worker to pickup Event type "+EventTypeId+" for realocation");
        waitForEventToBePickUpForRelocation();


        if(eventHasBeenPickedUpForRelocation(store,EventTypeId)) {
            if (logs_relo)
                System.out.println("Realocation | Waiting other worker to build state of Event type " + EventTypeId);
            waitForStateToBeRebuildedElseware(store, EventTypeId);

            acknolegeTransferConfirmed(store, EventTypeId);
            if (logs_relo)
                System.out.println("Realocation | Acknoledge tranfer of Event type " + EventTypeId + " to other worker");

            deleteCheckExpressionAndUnnecessaryInputs(store, EventTypeId);
            if (logs_relo)
                System.out.println("Realocation | Remove Event type " + EventTypeId + " check expression and inputs");

            store.Close();

            //update load balancer rank
            loadBalancer.deleteEventTypeofRank(EventTypeId, ReceiverList);
            if (logs_relo) System.out.println("Realocation | Remove Event type " + EventTypeId + " from load balancer");

            return true;
        }
        else {
            if(logs_relo )System.out.println("Realocation | Event type "+EventTypeId+" failed to be realocated");
            undoPutEventForRelocation(store,EventTypeId);
            return false;
        }

    }

    private void putEventForRelocation(Lettuce lettuce, double resourceusage, String EventTypeId){
        lettuce.pushEventTypeForRellocation(EventTypeId, resourceusage);
    }

    private void undoPutEventForRelocation(Lettuce lettuce, String EventTypeId){
        lettuce.undoPushEventTypeForRellocation(EventTypeId);
    }
    private void waitForEventToBePickUpForRelocation(){
        long t = 60000;
        if(logs_relo )System.out.println("Realocation | Waiting "+t+" microseconds");
        try {
            Thread.sleep(t);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

    }
    private boolean eventHasBeenPickedUpForRelocation(Lettuce lettuce, String EventTypeId){
        return lettuce.hasEventTypeBeenPickedUpForRellocation(EventTypeId,WorkerId);
    }
    private void waitForStateToBeRebuildedElseware(Lettuce store, String EventTypeId){
        String ReceveingWorkerID = store.getEventTypeReceiveingWorker(EventTypeId);
        boolean stateRebuiledElsewhere = store.eventTypeStateBuilded(EventTypeId,ReceveingWorkerID);
        while(!stateRebuiledElsewhere) {
            try {
                Thread.sleep(180000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            stateRebuiledElsewhere = store.eventTypeStateBuilded(EventTypeId,ReceveingWorkerID);
        }
    }
    private void acknolegeTransferConfirmed(Lettuce store, String OutputTypeID){
        String newWorker = store.getEventTypeReceiveingWorker(OutputTypeID);
        store.setEventTypeCurrentWorker(OutputTypeID,newWorker);
        store.removeEventTypeReceveingWorker(OutputTypeID);
    }
    private void deleteCheckExpressionAndUnnecessaryInputs(Lettuce store,String EventTypeId){
        //stop sending events
        if(logs_deletion) System.out.println("deleting event type check : "+EventTypeId);
        eventHandler.deleteCheckExpression(EventTypeId);

        //remove unnecessary input event types
        Set<String> EventTypeInputs = store.getEventTypeInputs(EventTypeId);
        Set<String> OtherEventTypes = ReceiverList.keySet();
        Set<String> UsedTypeNames = new HashSet<>();

        for (String input : EventTypeInputs){
            boolean isUsedbyAnotherEventType = false;
            for(String otherId : OtherEventTypes){
                if(store.getEventTypeInputs(otherId).contains(input))
                    isUsedbyAnotherEventType = true;
            }
            if(!isUsedbyAnotherEventType) {
                // remove unused inputs from eventHandler
                ReceiverList.get(input).CloseConnection();
                ReceiverList.remove(input);
            }
        }
        for(String InputTypeId : ReceiverList.keySet()){
            if(logs_deletion) System.out.println("deleting input type check : "+InputTypeId);
            UsedTypeNames.add(store.getEventTypeName(InputTypeId));
        }
        eventHandler.pruneInputs(UsedTypeNames);

        SenderList.remove(EventTypeId);
    }

    private void AddUnnasignedEventType(String EventTypeId, Lettuce store){
        store.eventTypeAssigned(EventTypeId);
        if(logs_add ) System.out.print("TH | Storing Event Type Id : "+EventTypeId+" as assigned\n");
        AddOutgoingEventType(EventTypeId,store);
    }


    private void AddOutgoingEventType(String OutputTypeID,Lettuce store){
        if(logs_add )System.out.println("TH | Adding Outgoing EventId : "+OutputTypeID);
        Set<String> inputTypes = store.getEventTypeInputs(OutputTypeID);
        if(logs_add )System.out.println("TH | Inputs for EventId "+OutputTypeID+" : "+inputTypes.toString());
        for(String inputTypeID : inputTypes){
            if(!ReceiverList.containsKey(inputTypeID)){
                if(logs_add ) System.out.print("TH | Adding Input EventId : "+inputTypeID+"\n");
                CreateInputReceiver(inputTypeID);
            }
        }
        String Name = store.getEventTypeName(OutputTypeID);
        String Query = store.getEventTypeDefinition(OutputTypeID);

        SenderList.put(OutputTypeID,NewSender());
        if(logs_add ) System.out.print("TH | Creating Sender for Event Type Id : "+OutputTypeID+", name : "+Name+"\n");
        eventHandler.addCheckExpression(OutputTypeID,Name, Query, SenderList.get(OutputTypeID));
        if(logs_add ) System.out.print("TH | Including Event Type Id : "+OutputTypeID+" on EventHandler\n");
        store.setAvroSchema(OutputTypeID,eventHandler.getSchema(OutputTypeID));
        if(logs_add ) System.out.print("TH | Storing Event Type Id : "+OutputTypeID+" current Avro Schema\n");
        store.setEventTypeCurrentWorker(OutputTypeID,WorkerId);
        if(logs_add ) System.out.print("TH | Storing Event Type Id : "+OutputTypeID+" current Worker\n");


        //update load balancer rank

        loadBalancer.addEventTypeToRank(OutputTypeID,ReceiverList);
        if(logs_add ) System.out.print("TH | Event Type "+Name+" is being detected\n");
    }

    private void CreateInputReceiver(String inputTypeID){
        Lettuce store = new Lettuce(RedisHost);
        String TypeName = store.getEventTypeName(inputTypeID);
        if(logs_add ) System.out.println("TH | Adding input type Name:"+TypeName+", Id :"+inputTypeID);
        Schema schema = store.getAvroSchema(inputTypeID);
        store.Close();
        eventHandler.addInputStream(TypeName,schema);
        if(logs_add ) System.out.println("TH | Adding input type to event handler");
        ReceiverList.put(inputTypeID,NewReceiver(TypeName,schema,inputTypeID,eventHandler));
        if(logs_add ) System.out.println("TH | Creating new receiver");
    }



    private Sender NewSender(){
        Sender sender;
        if(Broker.equals("Rabbitmq")){
            if(rmode.equals("conn"))  sender = new RabbitmqSender(connS);
            else if(rmode.equals("channel")) sender = new RabbitmqSender(channelS);
            else sender = new RabbitmqSender();
        }
        else sender = new NATSSender();
        return sender;
    }
    private Receiver NewReceiver(String TypeName, Schema schema, String TypeId, EventHandler eventHandler){
        Receiver receiver;
        if(Broker.equals("Rabbitmq")){
            if(rmode.equals("channel")) receiver = new RabbitmqReceiver(schema,TypeId,TypeName,eventHandler,channelR);
            else if(rmode.equals("conn")) receiver = new RabbitmqReceiver(schema,TypeId,TypeName,eventHandler,connR);
            else receiver = new RabbitmqReceiver(schema,TypeId,TypeName,eventHandler);

        }
        else receiver = new NATSReceiver(TypeName,schema,TypeId,eventHandler);
        return receiver;
    }

}
